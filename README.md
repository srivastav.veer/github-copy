# Introduction 

This project is a copy of GitHub UI. All the functionalities are not working.

The project is built using ReactJs and Bootstrap. It also contains font-awesome and some defined utility scss classes for better UI design Control. The SCSS files is placed in `{project_dir}/scss`. The SCSS file compiles and the compiled CSS should be placed inside `{project_dir}/src/css`.

The required font files are placed in  `{project_dir}/src/fonts`.

*Note*: Fonts and CSS should be placed relative to each other.

## Getting Started

Git clone the project or download the zip file. Open the project directory through command line and install package dependencies.

> npm install

Once packages are installed the application can be started in development mode.

> npm start dev