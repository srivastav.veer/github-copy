import axios from 'axios';

const Profile = {
    user: null,
    repo: null,
    loadUser: function (userId) {
        return new Promise(function (resolve, reject) {
            var that = this;
            axios({
                method: "get",
                url: 'https://api.github.com/users/'+userId,
                timeout: 10000
            }).then(function(response) {
                that.user = response.data;
                resolve();
            }).catch(e => {
                console.log(e.response);
                reject();
            });
        }.bind(this));
    },
    getUser: function () {
        return this.user;
    },
    loadRepos: function () {
        return new Promise(function (resolve, reject) {
            var that = this;
            axios({
                method: "get",
                url: 'https://api.github.com/users/'+that.user.login+'/repos',
                timeout: 10000
            }).then(function(response) {
                that.repo = response.data;
                resolve();
            }).catch(e => {
                console.log(e.response);
                reject();
            });
        }.bind(this));
    },
    getRepos: function() {
        return this.repo;
    }
}

export default Profile;