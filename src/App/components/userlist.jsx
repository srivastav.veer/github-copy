import React, {Fragment} from 'react';

export default function UserList(props) {

    return (
        <Fragment>
            <div className='container'>
                <div className='row'>
                    <div className='col col-12'>
                        <h5>Click of the below link to open a profile. Else feel free to change the URL</h5>
                        <ul>
                            <li><a href='/srivastav-veer'>Veer Shrivastav</a></li>
                            <li><a href='/supreetsingh247'>Supreet Singh</a></li>
                            <li><a href='/repeatedly'>Masahiro Nakagawa</a></li>
                            <li><a href='/onaluf'>Selim Arsever</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}