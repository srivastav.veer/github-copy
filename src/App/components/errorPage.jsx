import React, {Fragment} from 'react';

export default function ErrorPage(props) {

    return (
        <Fragment>
            <div className='container-flex whole'>
                <div className='content'>
                    <h1>404 Error! User Not Found</h1>
                </div>
            </div>
        </Fragment>
    );
}