import React, {Fragment} from 'react';
import {Button} from 'react-bootstrap';

import DetailsItem from './detailsItem';

export default function ProfileDetails(props) {
    var detailsToShow = {
        company: 'text',
        location: 'text',
        email: 'link',
        blog: 'link'
    };
    
    return (
        <Fragment>
            <div className='profile-details'>
                <h2 className='roboto'>{props.details.name}</h2>
                <h4 className='roboto-light'>{props.details.login}</h4>
            </div>
            <Button className='btn btn-block edit-profile'>Edit Profile</Button>
            {props.details.bio && 
                <div className='bio'>{props.details.bio}</div>
            }
            <ul className='details'>
                {Object.keys(detailsToShow).map((detail, key) => {
                    return (<DetailsItem key={key} type={detail} datatype={detailsToShow[detail]} value={props.details[detail]} />);
                })}
            </ul>
            <div className='text-sm margin-top-20'>Block or report user</div>
        </Fragment>
    );
}