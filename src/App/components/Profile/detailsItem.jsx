import React, {Fragment} from 'react';
import '../../../css/font-awesome/fontawesome.css';
import '../../../css/font-awesome/solid.css';

const bioclasses = {
    company: 'fas fa-user-friends',
    location: 'fas fa-map-marker-alt',
    email: 'fas fa-at',
    blog: 'fas fa-link'
};

export default function DetailsItem(props) {

    function render(val) {
        if (props.datatype === 'link') {
            return (
                <Fragment>
                    <a href={val} target="_blank" rel="noopener noreferrer">{val}</a>
                </Fragment>
            );
        }
        return val;
    }

    if (props.value) {
        return (
            <Fragment>
                <li className='roboto'><i className={bioclasses[props.type]}></i>{render(props.value)}</li>
            </Fragment>
        );
    } else {
        return (<Fragment></Fragment>);
    }
}