import React, { Fragment} from 'react';

export default function ProfilePicture(props) {
    return (
        <Fragment>
            <img src={props.pic} className='profile-pic' alt='profile pic' />
        </Fragment>
    );
}