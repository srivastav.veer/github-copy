import React, {Fragment, useState} from 'react';
import ProfilePicture from './Profile/picture';
import ProfileDetails from './Profile/details';
import ProfileNavigation from './MainLayouts/profile-navigation';
import InnerPage from './MainLayouts/pages/index';

export default function MainLayout(props) {

    const [page, selectPage] = useState('Overview');

    if (!props.user) {
        return <Fragment></Fragment>
    }

    function pageSelected (pageName) {
        selectPage(pageName);
    }

    return (
        <Fragment>
            <div className='container margin-top-20'>
                <div className='row'>
                    <div className='col col-12 col-md-4 col-lg-3 pr-md-3 pr-xl-6'>
                        <ProfilePicture pic={props.user.avatar_url} />
                        <ProfileDetails details={props.user} />
                    </div>
                    <div className='col col-12 col-md-8 col-lg-9'>
                        <ProfileNavigation onPage={(e) => pageSelected(e)} />
                        <InnerPage page={page} />
                    </div>
                </div>
            </div>
        </Fragment>
    );
}