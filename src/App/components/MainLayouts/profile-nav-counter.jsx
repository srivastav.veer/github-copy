import React, {Fragment, useState, useEffect} from 'react';
import ProfileService from '../../services/profile';
import axios from 'axios';

export default function ProfileNavCounter(props) {
    const [counter, setCounter] = useState(null);
    useEffect(() => {
        if ('callbackAPI' in props.data) {
            //TO DO Make AXios call here
            axios({
                method: "get",
                url: 'https://api.github.com/users/'+ProfileService.getUser().login+'/'+props.data.callbackAPI,
                timeout: 10000
            }).then(function(response) {
                setCounter(response.data.length);
            }).catch(e => {
                console.log(e.response);
            });
        }
    
        if ('counterDataKey' in props.data) {
            setCounter(ProfileService.getUser()[props.data.counterDataKey].toString())
        }
    }, []);

    if (!counter) {
        return (<Fragment></Fragment>);
    }

    return (
        <Fragment>
            <span className='margin-left-5 counter hide-lg hide-md hide-sm'>
                {counter}
            </span>
        </Fragment>
    );
}