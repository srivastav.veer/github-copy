import React, {Fragment} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import ProfileNavCounter from './profile-nav-counter';

function getJsonFromUrl(url) {
    if(!url) url = window.location.search;
    var query = url.substr(1);
    var result = {};
    query.split("&").forEach(function(part) {
      var item = part.split("=");
      result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
}

function updateQueryStringParameter(key, value) {
    let uri = window.location.origin + window.location.pathname;
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        window.location = uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        window.location = uri + separator + key + "=" + value;
    }
}

export default function ProfileNavigation(props) {
    var {tab} = getJsonFromUrl(window.location.search);
    if (!tab) {
        tab = '';
    }

    const tabs = {
        Overview: {link: ''},
        Repositories: {link: 'repositories', counterDataKey: 'public_repos'},
        Projects: {link: 'projects'},
        Stars: {link: 'stars', callbackAPI: 'starred'},
        Followers: {link: 'followers', counterDataKey: 'followers'},
        Following: {link: 'following', counterDataKey: 'following'}
    }

    return (
        <Fragment>
            <div className='profile-nav-holder'>
                <Navbar>
                    <Nav className="mr-auto">
                        {Object.keys(tabs).map((tabkey, key) => {
                            let className = '';
                            if (tabs[tabkey].link === tab) {
                                className='selected'
                                props.onPage(tabkey);
                            }

                            return (
                                <Nav.Link 
                                    key={key} 
                                    className={className+' mr-lg-3'} 
                                    onClick={() => updateQueryStringParameter('tab', tabs[tabkey].link)}
                                >
                                    {tabkey}
                                    <ProfileNavCounter data={tabs[tabkey]} />
                                </Nav.Link>)
                        })}
                    </Nav>
                </Navbar>
            </div>
        </Fragment>
    );
}