import React, {Fragment, Component} from 'react';
import { Form, Dropdown } from 'react-bootstrap';
import ProfileService from '../../../../services/profile';
import '../../../../../css/font-awesome/brands.css';
import RepoList from './ReposList';

export default class Repositories extends Component {

    constructor(props) {
        super(props);
        this.state= {
            repos: [],
            repoType: 'All',
            languages: ['All'],
            selectedLanguage: 'All'
        }

        this.repoTypes = {
            All: '',
            Sources: 'sources',
            Forks: 'fork',
            Archived: 'archived',
            Mirrors: 'mirror_url'
        }
    }

    componentDidMount() {
        ProfileService.loadRepos().then(function () {
            let lang = ['All'];
            ProfileService.getRepos().map(function (repo) {
                if (repo.language && !lang.includes(repo.language)) {
                    return lang.push(repo.language);
                }
            });
            console.log(lang);
            this.setState({repos: ProfileService.getRepos(), languages: lang});
        }.bind(this)).catch(e => {
            console.log(e);
        });
    }
    

    changeRepo(rule) {
        console.log('rule,', rule);
        if (rule === 'Sources') {
            this.getSources();
            this.setState({
                repoType: rule
            })
            return;
        }
        if (rule === 'All') {
            this.setState({
                repos: ProfileService.getRepos(),
                repoType: rule
            })
        }
        rule = this.repoTypes[rule];
        let sortedRepos = ProfileService.getRepos().filter(rep => {
            if (rule in rep && rep[rule]) {
                return true;
            }
        });
        this.setState({
            repos: sortedRepos,
            repoType: rule
        })
    }

    getSources() {
        let sortedRepos = ProfileService.getRepos().filter(rep => {
            if ('fork' in rep) {
                if (!rep['fork']) {
                    return true;
                }
            }
        });
        this.setState({
            repos: sortedRepos,
            repoType: 'Sources'
        })
    }

    languageSelect(lang) {
        if (lang === 'All') {
            this.setState({
                repos: ProfileService.getRepos(),
                selectedLanguage: lang
            })
            return;
        }

        let sortedRepos = ProfileService.getRepos().filter(rep => {
            if ('language' in rep && rep.language === lang) {
                return true;
            }
        });
        console.log(sortedRepos);
        this.setState({
            repos: sortedRepos,
            selectedLanguage: lang
        })
    }

    searchRepos(e) {
        let sortedRepos = ProfileService.getRepos().filter(repo => {
            return repo.name.toLowerCase().includes(e.target.value.toLowerCase());
        });
        this.setState({
            repos: sortedRepos,
        })
    }

    render() {
        return (
            <Fragment>
                <Form className='d-block d-md-flex margin-top-20 padding-bottom-20 underline'>
                    <div className='mb-3 mb-md-0 mr-md-3 flex-auto width-100'>
                        <Form.Control type="text" placeholder="Find a repository..." onChange={(e) => this.searchRepos(e)} />
                    </div>
                    <div className='d-flex'>
                        <Dropdown className='repo-dropdown' onSelect={(e) => this.changeRepo(e)}>
                            <Dropdown.Toggle variant="primary" id="dropdown-basic" className='right'>
                                Type: <span className='strong'>{this.state.repoType}</span>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {Object.keys(this.repoTypes).map((repot, key) => {
                                    return (<Dropdown.Item key={key} eventKey={repot}>{repot}</Dropdown.Item>)
                                })}
                            </Dropdown.Menu>
                        </Dropdown>
                        <Dropdown className='repo-dropdown margin-left-10' onSelect={(e) => this.languageSelect(e)}>
                            <Dropdown.Toggle variant="primary" id="dropdown-basic" className='right'>
                                Language: <span className='strong'>{this.state.selectedLanguage}</span>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {this.state.languages.map(function (value, key) {
                                    return (<Dropdown.Item key={key} eventKey={value}>{value}</Dropdown.Item>)
                                })}
                            </Dropdown.Menu>
                        </Dropdown>
                        <button className='right btn btn-success margin-left-10 new-btn'><i className="fab fa-git-alt"></i>New</button>
                    </div>
                </Form>
                <RepoList repos={this.state.repos} />
            </Fragment>
        )
    }
}

