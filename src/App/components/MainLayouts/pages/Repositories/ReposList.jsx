import React, {Fragment} from 'react';
import RepoListItem from './RepoListItem';

export default function RepoList(props) {
    return (
        <Fragment>
            <div className='row'>
                {props.repos.map((repo, key) => {
                    return (<RepoListItem repo={repo} key={key} />)
                })}
            </div>
        </Fragment>
    );
}