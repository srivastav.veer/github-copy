import React, {Fragment} from 'react';

export default function RepoListItem(props) {
    return (
        <Fragment>
            <div className='col-12 d-block width-100 py-4 border-bottom'>
                <h4 className='text-primary'>{props.repo.name}</h4>
            </div>
        </Fragment>
    );
}