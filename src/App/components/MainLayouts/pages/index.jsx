import React, {Fragment} from 'react';
import Repositories from './Repositories/index';

export default function InnerPage(props) {
    switch (props.page) {
        case 'Overview':
            return (<Fragment></Fragment>)
            break;
        case 'Repositories':
            return (<Repositories />)
            break;
        case 'Projects':
            return (<Fragment></Fragment>)
            break;
        case 'Stars':
            return (<Fragment></Fragment>)
            break;
        case 'Followers':
            return (<Fragment></Fragment>)
            break;
        case 'Following':
            return (<Fragment></Fragment>)
            break;
    }
}