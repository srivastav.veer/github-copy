import React, {Fragment, useState, useEffect} from 'react';
import '../css/app.css';
import ProfileService from './services/profile.js';

import Header from './components/header';
import ErrorPage from './components/errorPage';
import UserList from './components/userlist';
import MainUIHolder from './components/main-layout';

export default function Index() {

    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [index, setIndex] = useState(false);

    useEffect(() => {
        //An immediate invoked function to check the user from the URL.
        (function findProfile() {
            setLoading(true);
            let userId = window.location.pathname.split('/')[1];
            if (userId === '') {
                setIndex(true);
                return;
            }
            ProfileService.loadUser(userId).then(() => {
                setUser(ProfileService.getUser());
                setLoading(false);
            }).catch(e => {
                setError(true);
                setLoading(false);
            });
        })();
    }, []);

    if (error) {
        return (
            <Fragment>
                <ErrorPage />
            </Fragment>
        );
    }

    if (index) {
        return (
            <Fragment>
                <UserList />
            </Fragment>
        );
    }
    
    return (
        <Fragment>
            <Header />
            <MainUIHolder user={user} />
        </Fragment>
    );
}